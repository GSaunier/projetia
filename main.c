/*
	Canvas pour algorithmes de jeux à 2 joueurs
	
	joueur 0 : humain
	joueur 1 : ordinateur
			
*/
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

// Paramètres du jeu
#define LARGEUR_MAX 9 		// nb max de fils pour un noeud (= nb max de coups possibles)

#define TEMPS 1 // temps de calcul pour un coup avec MCTS (en secondes)

//Info jeu constantes
#define LARGEUR 7
#define HAUTEUR 6

#define CONST sqrt(2)

// macros
#define AUTRE_JOUEUR(i) (1-(i))
#define min(a, b)       ((a) < (b) ? (a) : (b))
#define max(a, b)       ((a) < (b) ? (b) : (a))


// Critères de fin de partie
typedef enum {NON, MATCHNUL, ORDI_GAGNE, HUMAIN_GAGNE } FinDePartie;

typedef enum {HAUT,BAS,GAUCHE,DROITE,HAUTDROIT,HAUTGAUCHE,BASDROIT,BASGAUCHE} Direction;

// Definition du type Coup
typedef struct {
    // TODO: à compléter par la définition d'un coup

    int colonne;

} Coup;

// Definition du type Etat (état/position du jeu)
typedef struct EtatSt {

    int joueur; // à qui de jouer ?

    Coup * dernierCoup;

    char plateau[HAUTEUR][LARGEUR];

} Etat;

// Nouveau coup
// TODO: adapter la liste de paramètres au jeu
Coup * nouveauCoup(int j ) {
    Coup * coup = (Coup *)malloc(sizeof(Coup));

    // TODO: à compléter avec la création d'un nouveau coup

    /* par exemple : */
    coup->colonne = j;

    return coup;
}

// Copier un état 
Etat * copieEtat( Etat * src ) {
    Etat * etat = (Etat *)malloc(sizeof(Etat));

    etat->joueur = src->joueur;

    etat->dernierCoup = nouveauCoup(src->dernierCoup->colonne);

    /* par exemple : */
    int i,j;
    for (i=0; i< HAUTEUR; i++)
        for ( j=0; j<LARGEUR; j++)
            etat->plateau[i][j] = src->plateau[i][j];



    return etat;
}

// Etat initial 
Etat * etat_initial( void ) {
    Etat * etat = (Etat *)malloc(sizeof(Etat));

    // TODO: à compléter avec la création de l'état initial

    /* par exemple : */
    int i,j;
    for (i=0; i< HAUTEUR; i++)
        for ( j=0; j<LARGEUR; j++)
            etat->plateau[i][j] = ' ';

    return etat;
}


void afficheJeu(Etat * etat) {

    // TODO: à compléter

    /* par exemple : */
    int i,j;
    printf("   |");
    for ( j = 0; j < LARGEUR; j++)
        printf(" %d |", j);
    printf("\n");
    printf("-----------------------------------");
    printf("\n");

    for(i=HAUTEUR-1; i >= 0; i--) {
        printf(" %d |", i);
        for ( j = 0; j < LARGEUR; j++)
            printf(" %c |", etat->plateau[i][j]);
        printf("\n");
        printf("---------------------------------");
        printf("\n");
    }
}

// Demander à l'humain quel coup jouer 
Coup * demanderCoup () {

    // TODO...

    /* par exemple : */
    int i;
    printf(" quelle colonne ? ") ;
    scanf("%d",&i);

    return nouveauCoup(i);
}

// Modifier l'état en jouant un coup 
// retourne 0 si le coup n'est pas possible
int jouerCoup( Etat * etat, Coup * coup ) {

    // TODO: à compléter

    /* par exemple : */
    int i = 0;
    while (i < HAUTEUR && etat->plateau[i][coup->colonne] != ' ')
        i++;

    if(i == HAUTEUR)
        return 0;
    else {
        etat->plateau[i][coup->colonne] = etat->joueur ? 'O' : 'X';
        etat->dernierCoup = nouveauCoup(coup->colonne);
        // à l'autre joueur de jouer
        etat->joueur = AUTRE_JOUEUR(etat->joueur);

        return 1;
    }
}

// Retourne une liste de coups possibles à partir d'un etat 
// (tableau de pointeurs de coups se terminant par NULL)
Coup ** coups_possibles( Etat * etat ) {

    Coup ** coups = (Coup **) malloc((1+LARGEUR_MAX) * sizeof(Coup *) );

    int k = 0;

    int i;
    for(i=0; i < LARGEUR; i++) {
        if(etat->plateau[HAUTEUR-1][i] == ' '){

            coups[k] = nouveauCoup(i);

            k++;
        }
    }

    /* fin de l'exemple */

    coups[k] = NULL;
    return coups;
}


// Definition du type Noeud 
typedef struct NoeudSt {

    int joueur; // joueur qui a joué pour arriver ici
    Coup * coup;   // coup joué par ce joueur pour arriver ici

    Etat * etat; // etat du jeu

    struct NoeudSt * parent;
    struct NoeudSt * enfants[LARGEUR_MAX]; // liste d'enfants : chaque enfant correspond à un coup possible
    int nb_enfants;	// nb d'enfants présents dans la liste

    // POUR MCTS:
    int nb_victoires;
    int nb_simus;

} Noeud;


// Créer un nouveau noeud en jouant un coup à partir d'un parent 
// utiliser nouveauNoeud(NULL, NULL) pour créer la racine
Noeud * nouveauNoeud (Noeud * parent, Coup * coup ) {
    Noeud * noeud = (Noeud *)malloc(sizeof(Noeud));

    if ( parent != NULL && coup != NULL ) {
        noeud->etat = copieEtat ( parent->etat );
        jouerCoup ( noeud->etat, coup );
        noeud->coup = coup;
        noeud->joueur = AUTRE_JOUEUR(parent->joueur);
    }
    else {
        noeud->etat = NULL;
        noeud->coup = NULL;
        noeud->joueur = 0;
    }
    noeud->parent = parent;
    noeud->nb_enfants = 0;

    // POUR MCTS:
    noeud->nb_victoires = 0;
    noeud->nb_simus = 0;


    return noeud;
}

// Ajouter un enfant à un parent en jouant un coup
// retourne le pointeur sur l'enfant ajouté
Noeud * ajouterEnfant(Noeud * parent, Coup * coup) {
    Noeud * enfant = nouveauNoeud (parent, coup ) ;
    parent->enfants[parent->nb_enfants] = enfant;
    parent->nb_enfants++;
    return enfant;
}

void freeNoeud ( Noeud * noeud) {
    if ( noeud->etat != NULL)
        free (noeud->etat);

    while ( noeud->nb_enfants > 0 ) {
        freeNoeud(noeud->enfants[noeud->nb_enfants-1]);
        noeud->nb_enfants --;
    }
    if ( noeud->coup != NULL)
        free(noeud->coup);

    free(noeud);
}

int compteurPion(char pion,int x,int y, Direction dir,Etat * etat){
    switch(dir) {
        case HAUT:
            x++;
            break;
        case BAS:
            x--;
            break;
        case GAUCHE:
            y--;
            break;
        case DROITE:
            y++;
            break;
        case HAUTDROIT:
            x++;
            y++;
            break;
        case HAUTGAUCHE:
            x++;
            y--;
            break;
        case BASDROIT:
            x--;
            y++;
            break;
        case BASGAUCHE:
            x--;
            y--;
            break;
    }
    if(x < HAUTEUR && x >= 0 && y < LARGEUR && y >= 0 && etat->plateau[x][y] == pion ) {
        return 1 + compteurPion(pion,x,y,dir,etat);
    }
    return 0;
}

// Test si l'état est un état terminal 
// et retourne NON, MATCHNUL, ORDI_GAGNE ou HUMAIN_GAGNE
FinDePartie testFin( Etat * etat ) {

    /* par exemple	*/

    // tester si un joueur a gagné

    int j = etat->dernierCoup->colonne;

    int i = HAUTEUR-1;

    while (i != -1 && etat->plateau[i][j] == ' ' ){
        i--;
    }


    int compteur = 0;
    char pion = etat->plateau[i][j];
    Direction dir[8] = {HAUT,BAS,GAUCHE,DROITE,HAUTDROIT,BASGAUCHE,HAUTGAUCHE,BASDROIT};
    int index = 0;
    while(index < 8 && compteur < 4 ) {
        compteur = 1;
        compteur += compteurPion(pion,i,j,dir[index],etat);
        compteur += compteurPion(pion,i,j,dir[index+1],etat);
        index+=2;
    }

    if(compteur>= 4){
        if(etat->joueur)
            return HUMAIN_GAGNE;
        else {
            return ORDI_GAGNE;
        }
    }

    index=0;
    while(index < LARGEUR && etat->plateau[HAUTEUR-1][index] != ' '){
        index++;
    }

    if(index == LARGEUR)
        return MATCHNUL;

    return NON;


}

Noeud * selection(Noeud * racine) {
    Noeud * temp, * select;
    float maxB = -1000000;
    float B,mu;
    int i = 0;

    //Si la racine n'a pas d'enfant on selectionne ce noeud
    if (racine->nb_enfants == 0) {
        return racine;
    }

    //On parcourt chaque noeud enfant on s'arrete si l'enfant n'est pas developpe sinon on prend le
    // noeud avec le plus grand B
    while(i < racine->nb_enfants) {
        if(racine->enfants[i]->nb_simus == 0) {
            return racine;
        }
        // calcul de mu moyenne des resultats
        mu =(float) (racine->enfants[i]->nb_victoires) /(float)(racine->enfants[i]->nb_simus);

        //ajout quand machine et moins quand joueur
        if (racine->enfants[i]->joueur)
            mu = -mu;
        //Calcul de B
        B = mu + CONST * sqrt(log(racine->nb_simus) / racine->enfants[i]->nb_simus);

        // On garde le meilleur
        if (B > maxB) {
            maxB = B;
            select = racine->enfants[i];
        }

        i++;
    }

    // On lance la selection dans le nouveau noeud pour parcourir les enfants
    return selection(select);
}

Noeud * developpement (Noeud * select) {
    if(select->nb_enfants == 0)
        return select;
    Noeud ** tab = malloc(select->nb_enfants* sizeof(Noeud *));
    int fils = 0;
    for (int i = 0; i < select->nb_enfants ; i++){
        if(select->enfants[i]->nb_simus == 0){
            tab[fils] = select->enfants[i];
            fils++;
        }
    }

    srand(time(NULL));
    int ra = rand()%fils;
    Noeud * enfant = tab[ra];
    free(tab);
    return enfant;
}

Noeud * simulation(Noeud * dev) {
    Noeud * simu = dev;
    Coup ** coups;
    srand(time(NULL));
    int k =-1;
    do{
        // créer les premiers noeuds:
        k = 0;
        if(testFin(simu->etat) == NON) {
            coups = coups_possibles(simu->etat);

            Noeud *enfant;
            while (coups[k] != NULL) {
                enfant = ajouterEnfant(simu, coups[k]);
                k++;
            }
            if (k != 0)
                simu = simu->enfants[rand() % k];
        }
    }
    while(k != 0);
    return simu;
}

void update(Noeud * fils) {
    Noeud * parent = fils;
    int r;

        if(testFin(fils->etat) == ORDI_GAGNE)
            r = 1;
        else
            r = 0;

    do {
        parent->nb_simus++;
        parent->nb_victoires+= r;
        parent = parent->parent;
    } while (parent != NULL);
}

Coup * bestCoup(Noeud * racine) {
    Noeud * temp, * selectRobuste, * selectMax;
    float mu,maxMu;
    int Ni, maxNi;
    int i = 0;
    maxMu = -1;
    maxNi = -1;

    //On parcourt chaque noeud enfant on s'arrete si l'enfant n'est pas developpe sinon on prend le
    // noeud avec le plus grand B
    while(i < racine->nb_enfants) {
        if(racine->enfants[i]->nb_simus!=0){
            temp = racine->enfants[i];
            Ni = racine->enfants[i]->nb_simus;
            // calcul de mu moyenne des resultats
            mu = (float)(temp->nb_victoires)/(float)(temp->nb_simus);

            // On garde le meilleur
            if(mu > maxMu){
                selectMax = temp;
                maxMu = mu;
            }

            if(Ni > maxNi) {
                selectRobuste = temp;
                maxNi = Ni;
            }
        }

        i++;
    }

    printf("Robuste\n");
    afficheJeu(selectRobuste->etat);
    printf("Probabilite de gagner: %f\n",maxMu);
    return selectMax->coup;
}

// Calcule et joue un coup de l'ordinateur avec MCTS-UCT
// en tempsmax secondes
void ordijoue_mcts(Etat * etat, int tempsmax) {

    clock_t tic, toc;
    tic = clock();
    int temps;

    Coup **coups;
    Coup *meilleur_coup = NULL;

    // Créer l'arbre de recherche
    Noeud *racine = nouveauNoeud(NULL, NULL);
    racine->etat = copieEtat(etat);

    // créer les premiers noeuds:
    coups = coups_possibles(racine->etat);
    int k = 0;
    Noeud *enfant;
    int gagnant = 0;
    while (coups[k] != NULL) {
        enfant = ajouterEnfant(racine, coups[k]);
        if(testFin(enfant->etat) == ORDI_GAGNE) {
            gagnant = 1;
            printf("Coup Gagnant!\n");
            meilleur_coup = coups[k];
        }
        k++;
    }

    /*  TODO :
        - supprimer la sélection aléatoire du meilleur coup ci-dessus
        - implémenter l'algorithme MCTS-UCT pour déterminer le meilleur coup ci-dessous
*/
    int iter = 0;


    if (gagnant == 0) {
        printf("Temps de reflexion: %ds\n", TEMPS);
        do {
            printf("Iteration: %d\n", iter);
            printf("Selection\n");
            Noeud *select = selection(racine);
            printf("Developpement\n");
            select = developpement(select);
            printf("Simulation\n");
            select = simulation(select);
            printf("Update\n");
            update(select);


            toc = clock();
            temps = (int) (((double) (toc - tic)) / CLOCKS_PER_SEC);
            iter++;
        } while (temps < tempsmax);


        /* fin de l'algorithme  */
        printf("Nombre de simulation: %d\n", iter);
        meilleur_coup = bestCoup(racine);
    }

    // Jouer le meilleur premier coup
    jouerCoup(etat, meilleur_coup );

    // Penser à libérer la mémoire :
    freeNoeud(racine);
    free (coups);

}

int main(void) {

    Coup * coup;
    FinDePartie fin;

    // initialisation
    Etat * etat = etat_initial();

    // Choisir qui commence :
    printf("Qui commence (0 : humain, 1 : ordinateur) ? ");
    scanf("%d", &(etat->joueur) );

    // boucle de jeu
    do {
        printf("\n");
        afficheJeu(etat);

        if ( etat->joueur == 0 ) {
            // tour de l'humain

            do {
                coup = demanderCoup();
            } while ( !jouerCoup(etat, coup) );

        }
        else {
            // tour de l'Ordinateur

            ordijoue_mcts( etat, TEMPS );

        }

        fin = testFin( etat );
    }	while ( fin == NON ) ;

    printf("\n");
    afficheJeu(etat);

    if ( fin == ORDI_GAGNE )
        printf( "** L'ordinateur a gagné **\n");
    else if ( fin == MATCHNUL )
        printf(" Match nul !  \n");
    else
        printf( "** BRAVO, l'ordinateur a perdu  **\n");
    return 0;
}
